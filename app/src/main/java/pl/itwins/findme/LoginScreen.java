package pl.itwins.findme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Task.UserLoginTask;

public class LoginScreen extends AppCompatActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    /**
     * Frontend login form validation
     */
    public void attemptLogin() {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            Map<String, Object> jsonParams = new HashMap<>();
            jsonParams.put("email", email);
            jsonParams.put("password", password);

            UserLoginTask mAuthTask = new UserLoginTask(this, jsonParams);
            mAuthTask.setViewVariables(mPasswordView);
            mAuthTask.execute((Void) null);
        }
    }

    public void showRegisterScreen(View v) {
        Intent intent = new Intent(this, RegisterScreen.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
        if(applicationHelper.getToken() != null){
            applicationHelper.redirectActivity(EventsScreen.class, null);
        }
    }
}
