package pl.itwins.findme.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.Task.SendLocationTask;

public class MyLocationService extends Service implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final long INTERVAL = ApplicationHelper.LOCATION_REFRESH_TIME;
    private static final long FASTEST_INTERVAL = ApplicationHelper.LOCATION_REFRESH_TIME;
    private static final String STORE_FILE_NAME = "current_event.bin";

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Event event = new Event();

    public MyLocationService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent != null){
            Bundle params = intent.getExtras();
            event = (Event) params.getSerializable("event");
            saveEvent(event);
        } else {
            event = loadEvent();
        }

        return START_STICKY;
    }

    private void saveEvent(Event event){
        try {
            FileOutputStream fos = openFileOutput(STORE_FILE_NAME, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(event);
            oos.flush();
            oos.close();
        }catch(Exception e) {
            Log.d("Monitor", "SAVE EVENT ERROR " + e.toString());
        }
    }

    private Event loadEvent(){
        Event event = new Event();
        try {
            FileInputStream fin = openFileInput(STORE_FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fin);
            event = (Event) ois.readObject();
        } catch (Exception e){
            Log.d("Monitor", "LOAD EVENT ERROR " + e.toString());
        }

        return event;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("Monitor", "Connection failed - " + connectionResult.toString());
    }


    @Override
    public void onConnected(Bundle arg0) {
        PendingResult<Status> pendingResult
                = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("Monitor", "Update location - event id " + event.getId());
        if(event.getId() != null) {
            try {
                event.setLongitude(String.valueOf(location.getLongitude()));
                event.setLatitude(String.valueOf(location.getLatitude()));
                SendLocationTask sendLocation = new SendLocationTask(event);
                sendLocation.execute((Void) null).get();
            } catch (Exception e) {
                Log.d("Monitor", "ERROR " + e.toString());
            }
        }
    }

    @Override
    public void onDestroy() {
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
    }

    protected void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }
}
