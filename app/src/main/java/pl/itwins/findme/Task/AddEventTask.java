package pl.itwins.findme.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;

import java.util.Map;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.MyEventsFragment;
import pl.itwins.findme.R;

public class AddEventTask extends AsyncTask<Void, Void, Boolean> {

    private Map<String, Object>  jsonEventParams;
    private Context context;
    private ProgressDialog progress;
    private FragmentActivity mActivity;

    public AddEventTask(FragmentActivity activity, Map<String, Object> eventParams) {
        this.jsonEventParams = eventParams;
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean addEventStatus = false;
        try {
            ApplicationApi api = new ApplicationApi();
            addEventStatus = api.addEvent(this.jsonEventParams);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addEventStatus;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.add_event_progress));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        progress.dismiss();
        if (success) {
            EditText eventNameView = (EditText) mActivity.findViewById(R.id.event_name);
            EditText eventDateView = (EditText) mActivity.findViewById(R.id.event_date);
            EditText eventTimeView = (EditText) mActivity.findViewById(R.id.event_time);

            eventNameView.setText("");
            eventDateView.setText("");
            eventTimeView.setText("");

            mActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.my_events_container, MyEventsFragment.newInstance())
                    .commit();

            new AlertDialog.Builder(this.mActivity)
                    .setMessage(this.context.getString(R.string.add_event_confirm))
                    .setPositiveButton("OK", null).show();
        } else {
            EditText eventNameView = (EditText) mActivity.findViewById(R.id.event_name);
            eventNameView.setError(this.context.getString(R.string.add_event_failed));
            eventNameView.requestFocus();
        }
    }
}