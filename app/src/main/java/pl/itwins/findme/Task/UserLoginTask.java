package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.DTO.PersonDTO;
import pl.itwins.findme.EventsScreen;
import pl.itwins.findme.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

    private Activity mActivity;
    private EditText mPasswordView;
    private Context context;
    private ProgressDialog progress;
    private Map<String, Object>  jsonEventParams;

    public UserLoginTask(Activity activity, Map<String, Object> eventParams) {
        this.jsonEventParams = eventParams;
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
    }

    public void setViewVariables(EditText passwordView) {
        mPasswordView = passwordView;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean loginStatus = false;
        try {

            ApplicationApi api = new ApplicationApi();
            JSONObject response = api.authenticate(this.jsonEventParams);
            JSONObject jsonObj = new JSONObject(response.toString());

            if (jsonObj.get("success").equals(true)) {
                ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
                applicationHelper.setToken(jsonObj.get("token").toString());
                applicationHelper.setUser(PersonDTO.jsonToPerson((JSONObject) jsonObj.get("user")));
                loginStatus = true;
            }

        } catch (JSONException e) {
            Log.d("Monitor:", "JSONException" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginStatus;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.login_progrss));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        progress.dismiss();
        if (success) {
            ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
            applicationHelper.redirectActivity(EventsScreen.class, null);
        } else {
            mPasswordView.setError(this.context.getString(R.string.error_incorrect_password));
            mPasswordView.requestFocus();
        }
    }
}