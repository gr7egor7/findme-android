package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.SharedEventsFragment;
import pl.itwins.findme.R;

public class GetSharedEventsTask extends AsyncTask<Void, Void, JSONArray> {

    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private Fragment fragment;

    public GetSharedEventsTask(Activity activity, Fragment contextFragment) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        fragment = contextFragment;
    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        JSONArray response = new JSONArray();
        try {
            ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
            ApplicationApi api = new ApplicationApi();
            response = api.getShareEvents(applicationHelper.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.refresh_shared_events));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(JSONArray result) {
        progress.dismiss();
        ((SharedEventsFragment)fragment).populateEvents(result);
    }
}