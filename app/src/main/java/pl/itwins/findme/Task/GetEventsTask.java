package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;

import org.json.JSONObject;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.MyEventsFragment;
import pl.itwins.findme.R;

public class GetEventsTask extends AsyncTask<Void, Void, JSONObject> {

    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private Fragment fragment;

    public GetEventsTask(Activity activity, Fragment contextFragment) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        fragment = contextFragment;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        JSONObject response = null;
        try {
            ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
            ApplicationApi api = new ApplicationApi();
            response = api.getEvents(applicationHelper.getUser().getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.refresh_events));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        progress.dismiss();
        ((MyEventsFragment)fragment).populateEvents(result);
    }
}