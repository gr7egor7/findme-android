package pl.itwins.findme.Task;

import android.os.AsyncTask;

import org.json.JSONObject;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.DTO.EventDTO;
import pl.itwins.findme.Entity.Event;

public class GetEventTask extends AsyncTask<Void, Void, Event> {

    private String eventId;

    public GetEventTask (String id){
        eventId = id;
    }

    @Override
    protected Event doInBackground(Void... params) {
        Event event;
        try {
            ApplicationApi api = new ApplicationApi();
            JSONObject response = api.getEvent(eventId);
            if(response != null) {
                event = EventDTO.jsonToEntity(response);
            } else {
                throw new Exception("");
            }

        } catch (Exception e) {
            event = new Event();
        }

        return event;
    }
}