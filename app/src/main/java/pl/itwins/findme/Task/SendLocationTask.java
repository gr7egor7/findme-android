package pl.itwins.findme.Task;

import android.os.AsyncTask;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.DTO.EventDTO;
import pl.itwins.findme.Entity.Event;

public class SendLocationTask extends AsyncTask<Void, Void, Boolean> {

    Event event;

    public SendLocationTask(Event event) {
        this.event = event;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean response = false;
        try {
            ApplicationApi api = new ApplicationApi();
            response = api.updateEvent(this.event.getId(), EventDTO.toJsonMap(event));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
}