package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;
import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.EventsScreen;
import pl.itwins.findme.LoginScreen;
import pl.itwins.findme.R;

public class UserRegisterTask extends AsyncTask<Void, Void, Boolean>  {


    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private Map<String, Object> jsonEventParams;
    private AutoCompleteTextView emailField;

    public UserRegisterTask(Activity activity, Map<String, Object> eventParams, AutoCompleteTextView emailField) {
        this.jsonEventParams = eventParams;
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        this.emailField = emailField;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean loginStatus = false;
        try {
            ApplicationApi api = new ApplicationApi();
            JSONObject response = api.register(this.jsonEventParams);
            JSONObject jsonObj = new JSONObject(response.toString());

            if (jsonObj.get("success").equals(true)) {
                loginStatus = true;
            }
        } catch (JSONException e) {
            Log.d("Monitor:", "JSONException" + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return loginStatus;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.register_progrss));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        progress.dismiss();
        if (success) {
            new AlertDialog.Builder(this.mActivity)
                    .setMessage(this.context.getString(R.string.register_progrss_complete))
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
                    applicationHelper.redirectActivity(LoginScreen.class, null);
                }
            }).show();

        } else {
            emailField.setError(this.context.getString(R.string.register_email_unique));
            emailField.requestFocus();
        }
    }
}
