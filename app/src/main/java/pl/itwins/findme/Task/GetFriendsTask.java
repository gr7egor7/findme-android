package pl.itwins.findme.Task;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONArray;
import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.MyFriendsFragment;
import pl.itwins.findme.R;

public class GetFriendsTask extends AsyncTask<Void, Void, JSONArray> {

    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private MyFriendsFragment fragment;


    public GetFriendsTask(Activity activity, MyFriendsFragment myFriendsFragment) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        fragment = myFriendsFragment;
    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        JSONArray response = null;
        try {

            ApplicationHelper applicationHelper = (ApplicationHelper) this.context;
            ApplicationApi api = new ApplicationApi();
            response = api.getFriends(applicationHelper.getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.search_friends_hint_panel));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(JSONArray result) {
        progress.dismiss();
        fragment.setResults(result);
    }

}
