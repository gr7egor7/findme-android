package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONArray;
import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.SearchFriendsFragment;
import pl.itwins.findme.R;

public class SearchFriendsTask extends AsyncTask<String, Void, JSONArray> {

    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private SearchFriendsFragment fragment;


    public SearchFriendsTask(Activity activity, SearchFriendsFragment searchFriendsFragment) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        fragment = searchFriendsFragment;
    }


    @Override
    protected JSONArray doInBackground(String... params) {
        JSONArray response = null;
        try {
            String phrase = "";
            if (params.length > 0) {
                phrase = params[0];
            }

            //ApplicationHelper applicationHelper = (ApplicationHelper) this.context;
            ApplicationApi api = new ApplicationApi();
            response = api.searchFriends(phrase);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.search_friends_hint_panel));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(JSONArray result) {
        progress.dismiss();
        fragment.setResults(result);
    }
}
