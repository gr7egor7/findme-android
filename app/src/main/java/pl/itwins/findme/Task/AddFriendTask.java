package pl.itwins.findme.Task;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Fragments.SearchFriendsFragment;
import pl.itwins.findme.R;

public class AddFriendTask  extends AsyncTask<String, Void, Boolean> {


    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private SearchFriendsFragment fragment;


    public AddFriendTask(Activity activity, SearchFriendsFragment searchFriendsFragment) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        fragment = searchFriendsFragment;
    }


    @Override
    protected Boolean doInBackground(String... params) {
        try {
            String nick = "";
            if (params.length > 0) {
                nick = params[0];
            }

            ApplicationHelper applicationHelper = (ApplicationHelper) this.context;
            ApplicationApi api = new ApplicationApi();
            return api.addFriend(nick, applicationHelper.getToken());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.add_friend_task));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        progress.dismiss();
        fragment.replaceFriendsView();
    }
}
