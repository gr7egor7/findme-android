package pl.itwins.findme.Task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.R;

public class ShareEventTask extends AsyncTask<Void, Void, Boolean> {

    private List<String> friendsList;
    private String evenId;
    private Context context;
    private ProgressDialog progress;
    private FragmentActivity mActivity;

    public ShareEventTask(FragmentActivity activity, String eventId, List<String> friends) {
        this.friendsList = friends;
        this.evenId = eventId;
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Boolean shareEventStatus = true;
        try {
            String token = ApplicationHelper.getInstance().getToken();
            ApplicationApi api = new ApplicationApi();
            shareEventStatus = api.shareEvent(evenId, friendsList, token);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return shareEventStatus;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.share_event_with_friends_progress));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        progress.dismiss();
        if (success) {

            new AlertDialog.Builder(this.mActivity)
                    .setMessage(this.context.getString(R.string.share_event_shared))
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //redirect ?
                        }
                    }).show();

        } else {
            new AlertDialog.Builder(this.mActivity)
                .setMessage(this.context.getString(R.string.share_event_failed))
                .setPositiveButton("OK", null).show();
        }
    }
}