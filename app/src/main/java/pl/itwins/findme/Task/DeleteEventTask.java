package pl.itwins.findme.Task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import org.json.JSONObject;

import pl.itwins.findme.Class.ApplicationApi;
import pl.itwins.findme.InterfaceAdapters.MyEventsListAdapter;
import pl.itwins.findme.R;

public class DeleteEventTask extends AsyncTask<Void, Void, JSONObject> {

    private Activity mActivity;
    private Context context;
    private ProgressDialog progress;
    private String eventId;
    private MyEventsListAdapter adapter;
    private int position;

    public DeleteEventTask(Activity activity, MyEventsListAdapter adapter, int position) {
        this.context = activity.getApplicationContext();
        this.mActivity = activity;
        this.eventId = adapter.getItem(position).getId();
        this.adapter = adapter;
        this.position = position;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        JSONObject response = null;
        try {
            ApplicationApi api = new ApplicationApi();
            response = api.deleteEvent(this.eventId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progress = new ProgressDialog(this.mActivity);
        progress.setMessage(this.context.getString(R.string.delete_event));
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        progress.dismiss();
        try {
            if (result != null && result.get("success").equals(true)) {
                adapter.removeListPosition(position);
                adapter.notifyDataSetChanged();
            } else {
                this.showErrorMessage();
            }
        } catch (Exception e) {
            this.showErrorMessage();
        }
    }

    protected void showErrorMessage(){
        new AlertDialog.Builder(this.mActivity)
                .setMessage(this.context.getString(R.string.event_delete_error))
                .setPositiveButton("OK", null).show();
    }
}