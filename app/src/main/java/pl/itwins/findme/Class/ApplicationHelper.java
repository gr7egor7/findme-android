package pl.itwins.findme.Class;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.LoginScreen;

public class ApplicationHelper extends Application {

    public static long LOCATION_REFRESH_TIME = 1000 * 10; //10 sec

    public String authorizationToken;
    public Person user;
    private static ApplicationHelper helperInstance;

    public String getToken() {
        return authorizationToken;
    }

    public void setToken(String str) {
        authorizationToken = str;
    }

    public static ApplicationHelper getInstance(){
        return helperInstance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        helperInstance = this;
    }

    public Person getUser() {
        return user;
    }

    public void setUser(Person userObj) {
        user = userObj;
    }

    public boolean verifyUser() {
        boolean status = false;
        ApplicationApi api = new ApplicationApi();

        if (!this.getUser().getId().isEmpty() && api.verifyToken(this.getToken())) {
            status = true;
        } else {
            redirectActivity(LoginScreen.class, null);
        }

        return status;
    }

    public void redirectActivity(Class activity, Bundle params) {
        Context context = getApplicationContext();
        Intent interfaceActivity = new Intent(context, activity);
        if(params != null) {
            interfaceActivity.putExtras(params);
        }
        interfaceActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(interfaceActivity);
    }
}
