package pl.itwins.findme.Class;

import android.os.NetworkOnMainThreadException;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ApplicationApi {

    public static String API_HOST = "http://nodejs-findmebeta.rhcloud.com";

    public boolean verifyToken(String token) {
        boolean status = false;
        try{
            HttpURLConnection urlConnection = this.getConnection("/verify", "POST");
            Map<String, Object> jsonParams = new HashMap<>();
            jsonParams.put("token", token);
            this.setJsonParams(urlConnection, jsonParams);
            JSONObject response = this.execute(urlConnection);
            if (response != null && response.get("success").equals(true)) {
                status = true;
            }
        } catch (JSONException e) {
            Log.d("Monitor:", "JSONException" + e.getMessage());
        }
        return status;
    }

    public JSONObject authenticate(Map<String, Object> jsonParams) {
        HttpURLConnection urlConnection = this.getConnection("/authenticate", "POST");
        this.setJsonParams(urlConnection, jsonParams);
        return this.execute(urlConnection);
    }

    public JSONObject getEvents(String userId) {
        HttpURLConnection urlConnection = this.getConnection("/events/" + userId, "GET");
        return this.execute(urlConnection);
    }

    public JSONObject getEvent(String eventId) {
        HttpURLConnection urlConnection = this.getConnection("/event/" + eventId, "GET");
        return this.execute(urlConnection);
    }

    public JSONObject deleteEvent(String eventId) {
        HttpURLConnection urlConnection = this.getConnection("/event/delete/" + eventId, "DELETE");
        return this.execute(urlConnection);
    }

    public JSONObject register(Map<String, Object> jsonParams) {
        HttpURLConnection urlConnection = this.getConnection("/user/register", "POST");
        this.setJsonParams(urlConnection, jsonParams);
        return this.execute(urlConnection);
    }

    public boolean addEvent(Map<String, Object> eventParams) {
        boolean status = false;
        try{
            HttpURLConnection urlConnection = this.getConnection("/event/add", "POST");
            this.setJsonParams(urlConnection, eventParams);
            JSONObject response = this.execute(urlConnection);
            if (response != null && response.get("success").equals(true)) {
                status = true;
            }
        } catch (JSONException e) {
            Log.d("Monitor:", "JSONException" + e.getMessage());
        }
        return status;
    }

    public boolean updateEvent(String eventId, Map<String, Object> eventParams) {
        boolean status = false;
        try{
            HttpURLConnection urlConnection = this.getConnection("/event/update/" + eventId, "PUT");
            this.setJsonParams(urlConnection, eventParams);
            JSONObject response = this.execute(urlConnection);
            if (response != null && response.get("success").equals(true)) {
                status = true;
            }
        } catch (JSONException e) {
            Log.d("Monitor:", "JSONException" + e.getMessage());
        }
        return status;
    }

    public JSONArray searchFriends(String phrase) {
        HttpURLConnection urlConnection = this.getConnection("/friends/search", "POST");
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("phrase", phrase);
        this.setJsonParams(urlConnection, jsonParams);
        return this.execute(urlConnection, JSONArray.class);
    }

    public boolean addFriend(String nick, String token) {
        HttpURLConnection urlConnection = this.getConnection("/friends/" + nick, "PUT");
        urlConnection.setRequestProperty("x-access-token", token);
        return this.created(urlConnection);
    }

    public JSONArray getFriends(String token) {
        HttpURLConnection urlConnection = this.getConnection("/friends", "GET");
        urlConnection.setRequestProperty("x-access-token", token);
        return this.execute(urlConnection, JSONArray.class);
    }

    public JSONArray getShareEvents(String token) {
        HttpURLConnection urlConnection = this.getConnection("/myshare", "GET");
        urlConnection.setRequestProperty("x-access-token", token);
        return this.execute(urlConnection, JSONArray.class);
    }

    public boolean shareEvent(String eventId, List<String> friends, String token) {
        try{
            HttpURLConnection urlConnection = this.getConnection("/event/share/"+eventId, "PUT");
            urlConnection.setRequestProperty("x-access-token", token);
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("friends", new JSONArray(friends));
            this.setJsonParam(urlConnection, jsonParam);
            return this.created(urlConnection);
        } catch (Exception e) {
            Log.d("Monitor:", "shareEvent exception " + e.getMessage());
        }

        return false;
    }

    private JSONObject execute(HttpURLConnection urlConnection) {
        return execute(urlConnection, JSONObject.class);
    }

    private <T> T execute(HttpURLConnection urlConnection, Class<T> responseType)
    {
        StringBuilder response  = new StringBuilder();
        T jsonObject = null;
        try{
            Log.d("Monitor:", urlConnection.getResponseCode() + "");
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                BufferedReader input = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()), 8192);
                String strLine = null;
                while ((strLine = input.readLine()) != null) {
                    response.append(strLine);
                }
                input.close();
                jsonObject = responseType.getDeclaredConstructor(String.class).newInstance(response.toString());
                Log.d("Monitor:", response.toString());
            }
        } catch (NetworkOnMainThreadException e) {
            Log.d("Monitor:", "NetworkOnMainThreadException execute " + e.toString());
        } catch (Exception e) {
            Log.d("Monitor:", "Exception execute " + e.toString());
        }
        return jsonObject;
    }


    private boolean created(HttpURLConnection urlConnection)
    {
        boolean response  = false;
        try{
            Log.d("Monitor:", urlConnection.getResponseCode() + "");
            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Response code: " + urlConnection.getResponseCode());
            }
            response = true;
        } catch (NetworkOnMainThreadException e) {
            Log.d("Monitor:", "NetworkOnMainThreadException execute " + e.toString());
        } catch (Exception e) {
            Log.d("Monitor:", "Exception execute " + e.toString());
        }
        return response;
    }


    private void setJsonParam(HttpURLConnection urlConnection, JSONObject jsonParam){
        try {
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonParam.toString());
            out.close();
        } catch (Exception e) {
            Log.d("Monitor:", "Exception json params " + e.getMessage());
        }
    }


    private void setJsonParams(HttpURLConnection urlConnection, Map<String, Object> jsonParams){
        try {
            JSONObject jsonParam = new JSONObject();

            for(String key: jsonParams.keySet()) {
                jsonParam.put(key, jsonParams.get(key));
            }

            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonParam.toString());
            out.close();
        } catch (Exception e) {
            Log.d("Monitor:", "Exception json params " + e.getMessage());
        }
    }


    private HttpURLConnection getConnection(String remote, String method) {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(API_HOST + remote);
            urlConnection = (HttpURLConnection) url.openConnection();
            //urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
        } catch (MalformedURLException e) {
            Log.d("Monitor:", "MalformedURLException" + e.toString());
        } catch (Exception e) {
            Log.d("Monitor:", "Exception connection " + e.toString());
        }
        return urlConnection;
    }

}
