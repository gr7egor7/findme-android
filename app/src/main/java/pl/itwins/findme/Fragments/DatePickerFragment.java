package pl.itwins.findme.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

import pl.itwins.findme.R;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        TextView eventDate = (TextView) getActivity().findViewById(R.id.event_date);
        String date = eventDate.getText().toString();
        if (!date.isEmpty()) {
            String[] timeParts = date.split("-");
            if(!timeParts[0].isEmpty() && !timeParts[1].isEmpty()  && !timeParts[2].isEmpty()) {
                day = Integer.parseInt(timeParts[0]);
                month = Integer.parseInt(timeParts[1]) - 1;
                year = Integer.parseInt(timeParts[2]);
            }
        }

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        TextView eventDate = (TextView) getActivity().findViewById(R.id.event_date);
        month++;
        String dayString = day + "";
        String monthString = month + "";

        if (day <= 9) {
            dayString = "0" + day;
        }

        if (month <= 9) {
            monthString = "0" + month;
        }

        eventDate.setText(dayString + "-" +  monthString + "-" + year);
        eventDate.setError(null);
    }
}
