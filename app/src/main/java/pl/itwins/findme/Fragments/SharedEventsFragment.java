package pl.itwins.findme.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import pl.itwins.findme.DTO.EventDTO;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.InterfaceAdapters.SharedEventsListAdapter;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.GetSharedEventsTask;

public class SharedEventsFragment extends Fragment {

    protected Activity mActivity;
    protected View view;

    public static SharedEventsFragment newInstance() {
        Bundle args = new Bundle();
        SharedEventsFragment fragment = new SharedEventsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = context instanceof Activity ? (Activity) context : null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.follow_events_tab, container, false);

        GetSharedEventsTask task = new GetSharedEventsTask(mActivity, this);
        task.execute((Void) null);
        return view;
    }

    public void populateEvents(JSONArray eventsArray)
    {
        try {
            if (eventsArray != null) {
                int length = eventsArray.length();
                if(length == 0) {
                    throw new Exception("no data found");
                }

                ArrayList<Event> eventList = new ArrayList<Event>();
                for (int i = 0; i < length; i++)
                {
                    JSONObject eventRow = eventsArray.optJSONObject(i);
                    eventList.add(EventDTO.jsonToEntity(eventRow));
                }
                SharedEventsListAdapter adapter = new SharedEventsListAdapter(eventList, this.getContext());

                ListView eventsList = (ListView) view.findViewById(R.id.shared_events_list);
                eventsList.setAdapter(adapter);
            } else {
                TextView eventInfo = (TextView) view.findViewById(R.id.shared_events_info);
                eventInfo.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            TextView eventInfo = (TextView) view.findViewById(R.id.shared_events_info);
            eventInfo.setVisibility(View.VISIBLE);
        }
    }
}