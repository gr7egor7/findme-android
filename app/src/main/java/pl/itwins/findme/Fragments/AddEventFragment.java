package pl.itwins.findme.Fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.AddEventTask;

public class AddEventFragment extends Fragment implements View.OnClickListener {

    private ApplicationHelper applicationHelper;

    public static AddEventFragment newInstance() {
        Bundle args = new Bundle();
        AddEventFragment fragment = new AddEventFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        applicationHelper = (ApplicationHelper) getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.generate_events_tab, container, false);
        defineUIListeners(view);
        return view;
    }

    public void defineUIListeners(View view) {
        EditText eventTime = (EditText)view.findViewById(R.id.event_time);
        eventTime.setOnClickListener(this);

        EditText evenDate = (EditText)view.findViewById(R.id.event_date);
        evenDate.setOnClickListener(this);

        view.findViewById(R.id.save_event_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateEvent(v);
            }
        });
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = null;
        switch (v.getId()) {
            case R.id.event_time:
                newFragment = new TimePickerFragment();
                newFragment.show(this.getFragmentManager(), "timePicker");
                break;
            case R.id.event_date:
                newFragment = new DatePickerFragment();
                newFragment.show(this.getFragmentManager(), "datePicker");
                break;
            default:
                break;
        }
    }



    public void generateEvent(View v) {
        EditText eventNameView = (EditText) getView().findViewById(R.id.event_name);
        EditText eventDateView = (EditText) getView().findViewById(R.id.event_date);
        EditText eventTimeView = (EditText) getView().findViewById(R.id.event_time);

        eventNameView.setError(null);
        eventDateView.setError(null);
        eventTimeView.setError(null);

        String eventName  = eventNameView.getText().toString();
        String eventDate = eventDateView.getText().toString();
        String eventTime  = eventTimeView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(eventName)) {
            eventNameView.setError(getString(R.string.error_field_required));
            focusView = eventNameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(eventDate)) {
            eventDateView.setError(getString(R.string.error_field_required));
            focusView = eventDateView;
            cancel = true;
        }

        if (TextUtils.isEmpty(eventTime)) {
            eventTimeView.setError(getString(R.string.error_field_required));
            focusView = eventTimeView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            String link = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 9);
            Map<String, Object> eventParams = new HashMap<>();
            eventParams.put("eventName", eventName);
            eventParams.put("eventDate", eventDate);
            eventParams.put("eventTime", eventTime);
            eventParams.put("userId", applicationHelper.getUser().getId());
            eventParams.put("eventLink", link);
            eventParams.put("active", "1");
            eventParams.put("latitude", "");
            eventParams.put("longitude", "");
            eventParams.put("userNick", applicationHelper.getUser().getNick());

            AddEventTask eventTask = new AddEventTask(getActivity(), eventParams);
            eventTask.execute((Void) null);
        }
    }
}