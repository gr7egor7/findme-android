package pl.itwins.findme.Fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.DTO.PersonDTO;
import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.EventsScreen;
import pl.itwins.findme.InterfaceAdapters.SearchViewAdapter;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.SearchFriendsTask;

public class SearchFriendsFragment extends Fragment {

    protected String phrase;

    public void search(String phrase) {
        this.phrase = phrase;
        searchExe();
    }

    @Override
    public void onStart() {
        super.onStart();
        searchExe();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_friends,container, false);
    }

    public void setResults(JSONArray people)
    {
        try {
            if (people != null) {
                int length = people.length();
                if(length == 0) {
                    throw new Exception("no data found");
                }

                Person user = ApplicationHelper.getInstance().getUser();
                ArrayList<Person> list = new ArrayList<>();
                for (int i = 0; i < length; i++)
                {
                    JSONObject row = people.optJSONObject(i);
                    Person person = PersonDTO.jsonToPerson(row);
                    if(!person.getNick().equals(user.getNick())) {
                        list.add(PersonDTO.jsonToPerson(row));
                    }
                }
                SearchViewAdapter adapter = new SearchViewAdapter(list, getContext(), getActivity(), this);

                ListView listview = (ListView) getView().findViewById(R.id.search_list);
                listview.setAdapter(adapter);
            }
        } catch (Exception e) {
        }

    }

    private void searchExe() {
        if (getActivity() != null) {
            SearchFriendsTask task = new SearchFriendsTask(getActivity(), this);
            task.execute(phrase);
        }
    }

    public void replaceFriendsView() {
        EventsScreen screen = (EventsScreen)getActivity();
        screen.replaceFragment(screen.getMyFriendsFragment());
    }

}
