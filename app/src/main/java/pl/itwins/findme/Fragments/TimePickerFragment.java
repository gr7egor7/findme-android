package pl.itwins.findme.Fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import pl.itwins.findme.R;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        TextView eventTime = (TextView) getActivity().findViewById(R.id.event_time);
        String time = eventTime.getText().toString();
        if (!time.isEmpty()) {
            String[] timeParts = time.split(":");
            if(!timeParts[0].isEmpty() && !timeParts[1].isEmpty()) {
                hour = Integer.parseInt(timeParts[0]);
                minute = Integer.parseInt(timeParts[1]);
            }
        }

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        TextView eventTime = (TextView) getActivity().findViewById(R.id.event_time);

        String hourString = hourOfDay + "";
        String minuteString = minute + "";

        if (hourOfDay <= 9) {
            hourString = "0" + hourOfDay;
        }

        if (minute <= 9) {
            minuteString = "0" + minute;
        }

        eventTime.setText(hourString + ":" +  minuteString);
        eventTime.setError(null);
    }
}