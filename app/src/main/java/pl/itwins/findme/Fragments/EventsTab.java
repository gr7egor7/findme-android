package pl.itwins.findme.Fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import pl.itwins.findme.InterfaceAdapters.PagerFragmentAdapter;
import pl.itwins.findme.R;

public class EventsTab extends Fragment {

    public ViewPager viewPager;
    public TabLayout tabLayout;
    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private FragmentManager fragmentManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events_tab, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);

        fragmentManager = getActivity().getSupportFragmentManager();
        fragmentList.add(0, MyEventsFragment.newInstance());
        fragmentList.add(1, SharedEventsFragment.newInstance());
        fragmentList.add(2, AddEventFragment.newInstance());
        PagerFragmentAdapter adapter = new PagerFragmentAdapter(fragmentManager, fragmentList, this.getContext());
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    //refresh shared events list every time you select the tab
                    fragmentManager.beginTransaction().replace(R.id.shared_container, SharedEventsFragment.newInstance()).commit();
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        return  view;
    }



}
