package pl.itwins.findme.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import pl.itwins.findme.DTO.EventDTO;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.InterfaceAdapters.MyEventsListAdapter;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.GetEventsTask;

public class MyEventsFragment extends Fragment implements View.OnClickListener {

    protected Activity mActivity;
    protected View view;

    public static MyEventsFragment newInstance() {
        Bundle args = new Bundle();
        MyEventsFragment fragment = new MyEventsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = context instanceof Activity ? (Activity) context : null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_events_tab, container, false);
        defineUIListeners(view);
        GetEventsTask task = new GetEventsTask(mActivity, this);
        task.execute((Void) null);
        return view;
    }

    public void populateEvents(JSONObject events)
    {
        try {
            if (events != null) {
                JSONArray eventsArray = (JSONArray) events.get("events");
                int length = eventsArray.length();
                if(length == 0) {
                    throw new Exception("no data found");
                }

                ArrayList<Event> eventList = new ArrayList<Event>();
                for (int i = 0; i < length; i++)
                {
                    JSONObject eventRow = eventsArray.optJSONObject(i);
                    eventList.add(EventDTO.jsonToEntity(eventRow));
                }
                MyEventsListAdapter adapter = new MyEventsListAdapter(eventList, this.getContext());

                ListView eventsList = (ListView) view.findViewById(R.id.events_list);
                eventsList.setAdapter(adapter);
            } else {
                TextView eventInfo = (TextView) view.findViewById(R.id.events_info);
                eventInfo.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            TextView eventInfo = (TextView) view.findViewById(R.id.events_info);
            eventInfo.setVisibility(View.VISIBLE);
        }

    }

    public void defineUIListeners(View view) {
    }

    @Override
    public void onClick(View v) {
        DialogFragment newFragment = null;
        switch (v.getId()) {
            default:
                break;
        }
    }
}