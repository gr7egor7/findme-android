package pl.itwins.findme.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.itwins.findme.DTO.FriendDTO;
import pl.itwins.findme.DTO.PersonDTO;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.Entity.Friend;
import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.InterfaceAdapters.MyFriendsAdapter;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.GetFriendsTask;
import pl.itwins.findme.Task.ShareEventTask;

public class MyFriendsFragment extends Fragment {

    private Boolean shareEventView = false;
    private Event event;
    List<Friend> friendsSelectionList;

    public MyFriendsFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        GetFriendsTask task = new GetFriendsTask(getActivity(),this);
        task.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle param = getArguments();
        if (param != null) {
            shareEventView = param.getBoolean("isSharedView");
            event = (Event) param.getSerializable("event");
        }

        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        if (shareEventView) {
            Button shareButton = (Button) view.findViewById(R.id.share_event_button);
            shareButton.setVisibility(View.VISIBLE);

            view.findViewById(R.id.share_event_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareEventDialog();
                }
            });
        }

        return view;
    }

    protected void shareEventDialog(){
        Context context = getActivity().getApplicationContext();

        new AlertDialog.Builder(getActivity())
                .setTitle(context.getString(R.string.share_evant_title))
                .setMessage(context.getString(R.string.share_evant_question))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        List<String> friends = FriendDTO.selectedFriendsToJsonListMap(friendsSelectionList);
                        ShareEventTask shareEventTask = new ShareEventTask(
                                getActivity(),
                                event.getId(),
                                friends
                        );
                        shareEventTask.execute((Void) null);
                    }
                }).setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        ).show();
    }

    public void setResults(JSONArray people)
    {
        try {
            if (people != null) {
                int length = people.length();
                if(length > 0) {
                    ArrayList<Person> list = new ArrayList<>();
                    for (int i = 0; i < length; i++)
                    {
                        JSONObject row = people.optJSONObject(i);
                        list.add(PersonDTO.jsonToPerson(row));
                    }
                    friendsSelectionList = list.get(0).getFriends();
                    setSharedFriendsChecked();
                    MyFriendsAdapter adapter = new MyFriendsAdapter(friendsSelectionList, getContext(), shareEventView);

                    ListView listview = (ListView) getView().findViewById(R.id.my_friends_list);
                    listview.setAdapter(adapter);
                }

            }
        } catch (Exception e) {
        }
    }


    private void setSharedFriendsChecked(){
        if (shareEventView) {
            List<String> friends = event.getFriends();
            for (int i = 0; i < friendsSelectionList.size(); i++) {
                if (friends.contains(friendsSelectionList.get(i).getName())) {
                    friendsSelectionList.get(i).setSelected(true);
                }
            }
        }
    }

}
