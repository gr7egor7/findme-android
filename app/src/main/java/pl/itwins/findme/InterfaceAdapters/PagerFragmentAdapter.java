package pl.itwins.findme.InterfaceAdapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import pl.itwins.findme.Fragments.MyEventsFragment;
import pl.itwins.findme.R;

public class PagerFragmentAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[];
    public ArrayList<Fragment> fragmentList = new ArrayList<>();
    public FragmentManager fragmentManager;

    public PagerFragmentAdapter(FragmentManager fm, ArrayList<Fragment> fragments, Context context) {
        super(fm);
        fragmentManager = fm;
        fragmentList = fragments;
        tabTitles = new String[] {
                context.getString(R.string.tab_one),
                context.getString(R.string.tab_two),
                context.getString(R.string.tab_three)
        };
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getItemPosition( Object object ) {

        if (object instanceof MyEventsFragment) {
            fragmentManager.beginTransaction().remove((Fragment) object).commit();
            return POSITION_NONE;
        }

        return super.getItemPosition(object);
    }
}