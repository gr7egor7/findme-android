package pl.itwins.findme.InterfaceAdapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.SharedLocationMapScreen;
import pl.itwins.findme.R;


public class SharedEventsListAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Event> list = new ArrayList<Event>();
    private Context context;
    private Activity activity;

    public SharedEventsListAdapter(ArrayList<Event> list, Context context) {
        this.list = list;
        this.context = context;
        this.activity = (Activity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Event getItem(int pos) {
        return list.get(pos);
    }

    public Object removeListPosition(int pos) {
        return list.remove(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.shared_event_row, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.shared_event_text);
        listItemText.setText(list.get(position).getEventName());

        //Handle buttons and add onClickListeners
        final ImageButton infoBtn = (ImageButton)view.findViewById(R.id.shere_event_info_btn);
        final ImageButton followBtn = (ImageButton)view.findViewById(R.id.shared_event_follow_btn);
        final ImageButton eventOptions = (ImageButton)view.findViewById(R.id.shared_event_settings);

        infoBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sharedEventInfoBox(activity, position);
            }
        });

        followBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
                    boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if(!isGPSEnabled) {
                        gpsSettingBox(activity);
                    } else {
                        startFollowLocationBox(activity, position);
                    }
                }
            }
        });

        eventOptions.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(infoBtn.getVisibility() == View.INVISIBLE) {
                    infoBtn.setVisibility(View.VISIBLE);
                    followBtn.setVisibility(View.VISIBLE);
                    eventOptions.setVisibility(View.INVISIBLE);
                }
                notifyDataSetChanged();
            }
        });

        view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(infoBtn.getVisibility() == View.INVISIBLE) {
                    infoBtn.setVisibility(View.VISIBLE);
                    followBtn.setVisibility(View.VISIBLE);
                    eventOptions.setVisibility(View.INVISIBLE);
                } else {
                    infoBtn.setVisibility(View.INVISIBLE);
                    followBtn.setVisibility(View.INVISIBLE);
                    eventOptions.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }

    protected void sharedEventInfoBox(Activity activity, int position) {

        Event event = this.getItem(position);

        String content = context.getString(R.string.shared_event_person) + " : " + event.getUserNick() + "\n\n"
                + context.getString(R.string.shared_event_name) + " : " + event.getEventName() + "\n\n"
                + context.getString(R.string.shared_event_date) + " : " + event.getEventDate() + "\n\n"
                + context.getString(R.string.shared_event_time) + " : " + event.getEventTime() + "\n";

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(context.getString(R.string.shared_info_title));
        alertDialog.setMessage(content);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    protected void gpsSettingBox(Activity activity) {
        final Activity mActivity = activity;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(context.getString(R.string.start_event_settings_title));
        alertDialog.setMessage(context.getString(R.string.start_event_settings));
        alertDialog.setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mActivity.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    protected void startFollowLocationBox(Activity activity, int position) {
        final Event event = this.getItem(position);
        final Activity mActivity = activity;

        new AlertDialog.Builder(activity)
                .setTitle(context.getString(R.string.follow_event_title))
                .setMessage(context.getString(R.string.follow_event))
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
                        Bundle params = new Bundle();
                        params.putString("eventId", event.getId());
                        applicationHelper.redirectActivity(SharedLocationMapScreen.class, params);
                    }
                })
                .setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }
}