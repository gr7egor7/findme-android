package pl.itwins.findme.InterfaceAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.Fragments.SearchFriendsFragment;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.AddFriendTask;


public class SearchViewAdapter  extends BaseAdapter implements ListAdapter {

    private ArrayList<Person> list = new ArrayList<>();
    private Context context;
    private Activity activity;
    private SearchFriendsFragment fragment;

    public SearchViewAdapter(ArrayList<Person> list, Context context, Activity activity, SearchFriendsFragment fragment) {
        this.list = list;
        this.context = context;
        this.activity = activity;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Person getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_person_row, null);
        }

        TextView textblock = (TextView) convertView.findViewById(R.id.row_text);
        String name = String.format("%s (%s)",getItem(position).getNick(), getItem(position).getEmail());
        textblock.setText(name);

        ImageButton button = (ImageButton) convertView.findViewById(R.id.event_add_friend_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriend(getItem(position).getNick());
            }
        });




        return convertView;
    }

    protected void addFriend(String nick) {
        AddFriendTask task = new AddFriendTask(activity, fragment);
        task.execute(nick);
    }
}
