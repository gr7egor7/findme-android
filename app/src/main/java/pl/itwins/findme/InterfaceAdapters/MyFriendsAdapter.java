package pl.itwins.findme.InterfaceAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import pl.itwins.findme.Entity.Friend;
import pl.itwins.findme.R;

public class MyFriendsAdapter   extends BaseAdapter implements ListAdapter {
    private List<Friend> list = new ArrayList<>();
    private Context context;
    private Boolean shareEventView = false;

    public MyFriendsAdapter(List<Friend> list, Context context, Boolean shareEventView) {
        this.list = list;
        this.context = context;
        this.shareEventView = shareEventView;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Friend getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friends_row, null);
        }

        TextView friendName = (TextView) convertView.findViewById(R.id.friend_row_name);
        friendName.setText(getItem(position).getName());

        //Button shareEvent = (Button) convertView.findViewById(R.id.share_event_button);
        //Log.d("Monitor", "button pressed " + shareEvent.getText());

        if (shareEventView) {
            CheckBox sharedChoice = (CheckBox) convertView.findViewById(R.id.share_choice);
            sharedChoice.setVisibility(View.VISIBLE);
            sharedChoice.setChecked(getItem(position).isSelected());

            sharedChoice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    getItem(position).setSelected(cb.isChecked());
                }
            });
        }

        return convertView;
    }

}
