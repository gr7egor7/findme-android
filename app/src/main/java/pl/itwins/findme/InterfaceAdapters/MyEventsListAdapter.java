package pl.itwins.findme.InterfaceAdapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.Fragments.MyFriendsFragment;
import pl.itwins.findme.MyLocationScreen;
import pl.itwins.findme.R;
import pl.itwins.findme.Task.DeleteEventTask;

public class MyEventsListAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Event> list = new ArrayList<Event>();
    private Context context;
    private Activity activity;

    public MyEventsListAdapter(ArrayList<Event> list, Context context) {
        this.list = list;
        this.context = context;
        this.activity = (Activity) context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Event getItem(int pos) {
        return list.get(pos);
    }

    public Object removeListPosition(int pos) {
        return list.remove(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //return list.get(pos).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.event_row, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.event_text);
        listItemText.setText(list.get(position).getEventName());

        //Handle buttons and add onClickListeners
        final ImageButton deleteBtn = (ImageButton)view.findViewById(R.id.event_delete_btn);
        final ImageButton startBtn = (ImageButton)view.findViewById(R.id.start_event);
        final ImageButton shareBtn = (ImageButton)view.findViewById(R.id.event_share_btn);
        final ImageButton eventOptions = (ImageButton)view.findViewById(R.id.event_settings);
        final MyEventsListAdapter adapter = this;

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                deleteEventBox(activity, adapter, position);
            }
        });

        startBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
                    boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if(!isGPSEnabled) {
                        gpsSettingBox(activity);
                    } else {
                        startSharingLocationBox(activity, position);
                    }
                }
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener(){
            final Event event = getItem(position);
            @Override
            public void onClick(View v) {
                FragmentActivity fragmentActivity = (FragmentActivity) activity;
                MyFriendsFragment myFriendsFragment = new MyFriendsFragment();
                Bundle param = new Bundle();
                param.putBoolean("isSharedView", true);
                param.putSerializable("event", event);
                myFriendsFragment.setArguments(param);
                fragmentActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, myFriendsFragment)
                        .commit();
            }
        });

        eventOptions.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(deleteBtn.getVisibility() == View.INVISIBLE) {
                    deleteBtn.setVisibility(View.VISIBLE);
                    startBtn.setVisibility(View.VISIBLE);
                    shareBtn.setVisibility(View.VISIBLE);
                    eventOptions.setVisibility(View.INVISIBLE);
                }
                notifyDataSetChanged();
            }
        });

        view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(deleteBtn.getVisibility() == View.INVISIBLE) {
                    deleteBtn.setVisibility(View.VISIBLE);
                    startBtn.setVisibility(View.VISIBLE);
                    shareBtn.setVisibility(View.VISIBLE);
                    eventOptions.setVisibility(View.INVISIBLE);
                } else {
                    deleteBtn.setVisibility(View.INVISIBLE);
                    startBtn.setVisibility(View.INVISIBLE);
                    shareBtn.setVisibility(View.INVISIBLE);
                    eventOptions.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }

    protected void startSharingLocationBox(Activity activity, int position) {
        final Activity mActivity = activity;
        final Event event = this.getItem(position);
        new AlertDialog.Builder(mActivity)
                .setTitle(context.getString(R.string.start_event_title))
                .setMessage(context.getString(R.string.start_event_confirmation))
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ApplicationHelper applicationHelper = ApplicationHelper.getInstance();
                        Bundle params = new Bundle();
                        params.putSerializable("event", event);
                        applicationHelper.redirectActivity(MyLocationScreen.class, params);
                    }
                })
                .setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    protected void gpsSettingBox(Activity activity) {
        final Activity mActivity = activity;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(context.getString(R.string.start_event_settings_title));
        alertDialog.setMessage(context.getString(R.string.start_event_settings));
        alertDialog.setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mActivity.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    protected void deleteEventBox(Activity activity, MyEventsListAdapter adapter, int position) {
        final Activity mActivity = activity;
        final MyEventsListAdapter mAdapter = adapter;
        final int mPosition = position;
        new AlertDialog.Builder(mActivity)
                .setTitle(context.getString(R.string.delete_confirmation_title))
                .setMessage(context.getString(R.string.delete_confirmation))
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        DeleteEventTask deleteTask = new DeleteEventTask(
                                mActivity,
                                mAdapter,
                                mPosition
                        );
                        deleteTask.execute((Void) null);
                    }
                })
                .setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

}