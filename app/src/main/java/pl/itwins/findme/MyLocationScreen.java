package pl.itwins.findme;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.Services.MyLocationService;
import pl.itwins.findme.Entity.Event;

public class MyLocationScreen extends AppCompatActivity {

    ApplicationHelper applicationHelper;
    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applicationHelper = ApplicationHelper.getInstance();

        //get event id param
        Bundle params = getIntent().getExtras();
        event = (Event) params.getSerializable("event");
        if (event == null || event.getId().isEmpty()) {
            noEventId();
        }

        if (!isGooglePlayServicesAvailable()) {
            googlePlayServicesUnavailable();
        }

        setContentView(R.layout.activity_my_location_screen);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cacaca")));

        Intent serviceIntent = new Intent(this, MyLocationService.class);
        Bundle param = new Bundle();
        param.putSerializable("event", event);
        serviceIntent.putExtras(param);

        startService(serviceIntent);
    }

    protected void stopSendingLocation()
    {
        stopService(new Intent(this, MyLocationService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_menu_intrface, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings_logout) {
            stopSendingLocation();
            applicationHelper.setToken(null);
            applicationHelper.setUser(null);
            applicationHelper.redirectActivity(LoginScreen.class, null);
        }
        if (id == R.id.action_show_events) {
            backToEventsDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        backToEventsDialog();
    }

    protected void backToEventsDialog(){
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.my_location_finish_title))
                .setMessage(context.getString(R.string.my_location_finish_question))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        stopSendingLocation();
                        applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        ).show();
    }

    protected void noEventId() {
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.map_follow_problem_title))
                .setMessage(context.getString(R.string.no_event_id))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).show();
    }

    protected void googlePlayServicesUnavailable() {
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.map_follow_problem_title))
                .setMessage(context.getString(R.string.no_play_service_available))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        ApplicationHelper helper = ApplicationHelper.getInstance();
        savedInstanceState.putSerializable("user", helper.getUser());
        savedInstanceState.putString("token", helper.getToken());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ApplicationHelper helper = ApplicationHelper.getInstance();
        helper.setToken(savedInstanceState.getString("token"));
        helper.setUser((Person) savedInstanceState.getSerializable("user"));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSendingLocation();
    }
}
