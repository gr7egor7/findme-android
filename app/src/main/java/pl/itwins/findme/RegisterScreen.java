package pl.itwins.findme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import java.util.HashMap;
import java.util.Map;

import pl.itwins.findme.Task.UserLoginTask;
import pl.itwins.findme.Task.UserRegisterTask;

public class RegisterScreen extends AppCompatActivity {

    private AutoCompleteTextView mEmailView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
    }

    public void execute(View v) {
        AutoCompleteTextView emailField =  (AutoCompleteTextView)findViewById(R.id.email);
        EditText passwordField = (EditText)findViewById(R.id.password);
        EditText re_passwordField = (EditText)findViewById(R.id.re_password);
        EditText firstNameField = (EditText)findViewById(R.id.firstName);
        EditText lastNameField = (EditText)findViewById(R.id.lastName);
        EditText nick = (EditText)findViewById(R.id.nick);

        Boolean error = false;

        if (TextUtils.isEmpty(nick.getText().toString())) {
            nick.setError(getString(R.string.error_field_required));
            error = true;
        }

        if (TextUtils.isEmpty(emailField.getText().toString())) {
            emailField.setError(getString(R.string.error_field_required));
            error = true;
        } else if(!Patterns.EMAIL_ADDRESS.matcher(emailField.getText().toString()).matches()) {
            emailField.setError(getString(R.string.error_invalid_email));
            error = true;
        }
        if (TextUtils.isEmpty(passwordField.getText().toString())) {
            passwordField.setError(getString(R.string.error_field_required));
            error = true;
        } else {

            if (passwordField.getText().toString().length() < 7) {
                passwordField.setError(getString(R.string.error_invalid_password));
                error = true;
            }

            if (!passwordField.getText().toString().equals(re_passwordField.getText().toString())) {
                re_passwordField.setError(getString(R.string.error_diff_password));
                error = true;
            }
        }

        if (error) {
            v.requestFocus();
        } else {
            Map<String, Object> jsonParams = new HashMap<>();
            jsonParams.put("email", emailField.getText().toString());
            jsonParams.put("password", passwordField.getText().toString());
            jsonParams.put("firstName", firstNameField.getText().toString());
            jsonParams.put("lastName", lastNameField.getText().toString());
            jsonParams.put("nick", nick.getText().toString());

            UserRegisterTask mAuthTask = new UserRegisterTask(this, jsonParams, emailField);
            mAuthTask.execute((Void) null);
        }
    }
}
