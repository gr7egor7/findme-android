package pl.itwins.findme;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.Fragments.EventsTab;
import pl.itwins.findme.Fragments.MyFriendsFragment;
import pl.itwins.findme.Fragments.SearchFriendsFragment;

public class EventsScreen extends AppCompatActivity{

    private ApplicationHelper applicationHelper;
    private EventsTab eventsTab;
    private MyFriendsFragment myFriendsFragment;
    private SearchFriendsFragment searchFriendsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_screen);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cacaca")));

        applicationHelper = (ApplicationHelper)getApplicationContext();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new EventsTab())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_intrface, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search_friends).getActionView();
        searchView.setQueryHint(getString(R.string.search_friends_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return callSearch(query);
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() >= 5) {
                    return callSearch(newText);
                }
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    public boolean callSearch(String text) {
        replaceFragment(getSearchFriendsFragment());
        getSearchFriendsFragment().search(text);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_settings_logout:
                applicationHelper.setToken(null);
                applicationHelper.setUser(null);
                applicationHelper.redirectActivity(LoginScreen.class, null);
            case R.id.action_show_events:
                replaceFragment(getEventsTab());
                break;
            case R.id.action_show_friends:
                replaceFragment(getMyFriendsFragment());
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        ApplicationHelper helper = ApplicationHelper.getInstance();
        savedInstanceState.putSerializable("user", helper.getUser());
        savedInstanceState.putString("token", helper.getToken());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ApplicationHelper helper = ApplicationHelper.getInstance();
        helper.setToken(savedInstanceState.getString("token"));
        helper.setUser((Person) savedInstanceState.getSerializable("user"));
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    public EventsTab getEventsTab() {
        eventsTab = new EventsTab();
        return eventsTab;
    }

    public MyFriendsFragment getMyFriendsFragment() {
        if (myFriendsFragment == null) {
            myFriendsFragment = new MyFriendsFragment();
        }

        return myFriendsFragment;
    }

    public SearchFriendsFragment getSearchFriendsFragment() {
        if (searchFriendsFragment == null) {
            searchFriendsFragment = new SearchFriendsFragment();
        }
        return searchFriendsFragment;
    }
}
