package pl.itwins.findme.DTO;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.itwins.findme.Entity.Friend;
import pl.itwins.findme.Entity.Person;

public class PersonDTO {

    public static Person jsonToPerson(JSONObject json)
    {
        Person person = new Person();
        try {
            person.setId(json.has("_id") ? json.get("_id").toString() : "");
            person.setEmail(json.has("email") ? json.get("email").toString() : "");
            person.setFirstName(json.has("firstName") ? json.get("firstName").toString() : "");
            person.setLastName(json.has("lastName") ? json.get("lastName").toString() : "");
            person.setNick(json.has("nick") ? json.get("nick").toString() : "");
            if (json.has("friends")) {
                person.setFriends(convertPeopleToList(json.getJSONArray("friends")));
            }
        } catch (Exception e) {

        }
        return person;
    }

    private static List<Friend> convertPeopleToList(JSONArray friends) {
        List<Friend> list = new ArrayList<>();
        for (int i = 0; i < friends.length(); i++)
        {
            try {
                Friend friend = new Friend();
                friend.setName(friends.getString(i));
                friend.setSelected(false);
                list.add(friend);
            } catch (Exception ex) {

            }
        }
        return list;
    }

}
