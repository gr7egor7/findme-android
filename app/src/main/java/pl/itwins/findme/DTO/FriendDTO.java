package pl.itwins.findme.DTO;

import java.util.ArrayList;
import java.util.List;;

import pl.itwins.findme.Entity.Friend;


public class FriendDTO {

    public static List<String> selectedFriendsToJsonListMap(List<Friend> selectedFriends)
    {
        final int selectedFriendsLength = selectedFriends.size();
        List<String> mapParams = new ArrayList<String>();
        for (int i = 0; i < selectedFriendsLength; i++)
        {
            if (selectedFriends.get(i).isSelected()){
                mapParams.add(selectedFriends.get(i).getName());
            }
        }

        return mapParams;
    }
}
