package pl.itwins.findme.DTO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import pl.itwins.findme.Entity.Event;

public class EventDTO {

    public static Event jsonToEntity(JSONObject jsonEvent)
    {
        Event event = new Event();
        try {
            event.setId(jsonEvent.get("_id").toString());
            event.setEventDate(jsonEvent.get("eventDate").toString());
            event.setEventTime(jsonEvent.get("eventTime").toString());
            event.setEventName(jsonEvent.get("eventName").toString());
            event.setEventLink(jsonEvent.get("eventLink").toString());
            event.setLatitude(jsonEvent.get("latitude").toString());
            event.setLongitude(jsonEvent.get("longitude").toString());
            event.setUserId(jsonEvent.get("userId").toString());
            event.setActive(jsonEvent.get("active").toString());
            event.setUserNick(jsonEvent.get("userNick").toString());
            if (jsonEvent.has("friends")) {
                event.setFriends(convertPeopleToList(jsonEvent.getJSONArray("friends")));
            }
        } catch (Exception e) {

        }

        return event;
    }

    private static List<String> convertPeopleToList(JSONArray friends) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < friends.length(); i++)
        {
            try {
                list.add(friends.getString(i));
            } catch (Exception ex) {

            }
        }
        return list;
    }

    public static Map<String, Object> toJsonMap(Event event)
    {
        Map<String, Object> mapParams = new HashMap<>();
        try {
            mapParams.put("_id", event.getId());
            mapParams.put("eventDate", event.getEventDate());
            mapParams.put("eventTime", event.getEventTime());
            mapParams.put("eventName", event.getEventName());
            mapParams.put("eventLink", event.getEventLink());
            mapParams.put("active", event.getActive());
            mapParams.put("userId", event.getUserId());
            mapParams.put("latitude", event.getLatitude());
            mapParams.put("longitude", event.getLongitude());
            mapParams.put("userNick", event.getUserNick());
            mapParams.put("friends", new JSONArray(event.getFriends()));
        } catch (Exception e) {

        }

        return mapParams;
    }
}
