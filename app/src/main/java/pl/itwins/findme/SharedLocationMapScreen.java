package pl.itwins.findme;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.Marker;

import pl.itwins.findme.Class.ApplicationHelper;
import pl.itwins.findme.Entity.Event;
import pl.itwins.findme.Entity.Person;
import pl.itwins.findme.Task.GetEventTask;

public class SharedLocationMapScreen extends AppCompatActivity implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final long INTERVAL = ApplicationHelper.LOCATION_REFRESH_TIME;
    private static final long FASTEST_INTERVAL = ApplicationHelper.LOCATION_REFRESH_TIME;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    GoogleMap googleMap;
    Marker sharedLocation;
    ApplicationHelper applicationHelper;
    Boolean initialUpdateLocation = true;
    String eventId;
    Boolean isGooglePlayServicesAvailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        applicationHelper = ApplicationHelper.getInstance();

        //get event id param
        Bundle params = getIntent().getExtras();
        eventId = params.getString("eventId");
        if (eventId == null || eventId.isEmpty()) {
            noEventId();
        }

        isGooglePlayServicesAvailable = isGooglePlayServicesAvailable();
        if (!isGooglePlayServicesAvailable) {
            googlePlayServicesUnavailable();
        } else {
            setContentView(R.layout.activity_shared_location_map__screen);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#cacaca")));

            createLocationRequest();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            googleMap = fm.getMap();
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_menu_intrface, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings_logout) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
            applicationHelper.setToken(null);
            applicationHelper.setUser(null);
            applicationHelper.redirectActivity(LoginScreen.class, null);
        }
        if (id == R.id.action_show_events) {
            backToEventsDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isGooglePlayServicesAvailable) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isGooglePlayServicesAvailable) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        PendingResult<Status> pendingResult
                = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("Monitor", "Connection failed - " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("Monitor", "Update position " + location.toString());
        if (initialUpdateLocation) {
            markMyLocation(location);
        }
        markSharedLocation(location);
        initialUpdateLocation = false;
    }

    protected void markMyLocation(Location location)
    {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }

    protected void markSharedLocation(Location location)
    {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        Boolean foundLocation = true;
        String eventName = "";
        String eventDescription = "";

        try {
            GetEventTask getEventTask = new GetEventTask(eventId);
            Event event = getEventTask.execute((Void) null).get();

            eventDescription = getApplicationContext().getString(R.string.description_day) + ": "
                    + event.getEventDate() + ", " + getApplicationContext().getString(R.string.description_time)
                    + ": " + event.getEventTime();

            latitude = Double.parseDouble(event.getLatitude());
            longitude = Double.parseDouble(event.getLongitude());
            eventName = event.getEventName();
        } catch (Exception e){
            foundLocation = false;
        }

        if (foundLocation) {
            if(sharedLocation != null){
                sharedLocation.remove();
            }

            LatLng latLng = new LatLng(latitude, longitude);
            sharedLocation = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(eventName)
                    .snippet(eventDescription)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

            if (initialUpdateLocation) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        if(isGooglePlayServicesAvailable && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable && mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    public void onBackPressed() {
        backToEventsDialog();
    }

    protected void backToEventsDialog(){
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.follow_finish_title))
                .setMessage(context.getString(R.string.follow_finish_question))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }
        ).show();
    }

    protected void noEventId() {
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.map_follow_problem_title))
                .setMessage(context.getString(R.string.no_event_id))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).show();
    }

    protected void googlePlayServicesUnavailable() {
        Context context = getApplicationContext();

        new AlertDialog.Builder(this)
                .setTitle(getApplicationContext().getString(R.string.map_follow_problem_title))
                .setMessage(context.getString(R.string.no_play_service_available))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //applicationHelper.redirectActivity(EventsScreen.class, null);
                    }
                }).show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        ApplicationHelper helper = ApplicationHelper.getInstance();
        savedInstanceState.putSerializable("user", helper.getUser());
        savedInstanceState.putString("token", helper.getToken());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ApplicationHelper helper = ApplicationHelper.getInstance();
        helper.setToken(savedInstanceState.getString("token"));
        helper.setUser((Person) savedInstanceState.getSerializable("user"));
    }

}
